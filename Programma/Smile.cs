﻿using System;
using System.Drawing;
using System.Windows.Forms;

class Smile : Form
{
    // The screen and its properties.
    Graphics screen;

    // Dimensions of the screen.
    int sizeX = 800;
    int sizeY = 600;

    #region Other components

    static void Main()
    {
        Application.Run(new Smile());
    }

    Smile()
    {
        // Set the dimensions of the painting canvas.
        this.Size   = new Size(sizeX, sizeY);

        // Give the painting canvas a name.
        this.Text   = "Smile";

        // Call the painting method to paint on the canvas.
        this.Paint += (object o, PaintEventArgs p) => { this.screen = p.Graphics; this.Draw(); };
    }

    #endregion

    void Draw()
    {
        screen.FillRectangle(Brushes.Blue          , 50,  50, 200, 100);
        screen.DrawLine     (new Pen(Color.Red, 5) , 50, 160, 250, 260);
    }
}