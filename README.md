# Smiley Practicum
Bevat de source code voor het smiley practicum van de meeloopdag.

# Het idee
Let op: zorg ervoor dat je niet vanuit het .zipje werkt! Pak altijd het .zipje uit.  

Zorg ervoor dat de volgende dingen open staan:  
 * Visual Studio, met het code bestand Smile.cs open.
 * Het bestand 'Presentatie.pdf'.
 * Het bestand 'Smile-practicum.pdf'.
 